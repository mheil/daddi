all:
	pdflatex cusp.tex
	bibtex cusp
	pdflatex cusp.tex
	pdflatex cusp.tex

diff:
#	latexdiff cusp_old.tex cusp.tex > diff_raw.tex
#	cat diff_raw.tex | tr -d '\015' > diff.tex	
#	pdflatex diff.tex
#	pdflatex diff.tex

backup:
	tar cvfz ../backup.tar.gz *; mv ../backup.tar.gz .

clean:
	rm -f cusp.aux  cusp.bbl  cusp.blg  cusp.dvi  cusp.log  cusp.pdf *~
